<?php

namespace App\DataFixtures;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Answer;
use App\Entity\Question;



class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 20; $i++) {
            $question = new Question();
            $question->setQuestionText('question '.$i);
            $manager->persist($question);
       
            $answer = new Answer();
            $answer->setAnswerText('answer '.$i);
            $answer->setQuestion($question);
            $manager->persist($answer);
        }


        $manager->flush();
    }
}
