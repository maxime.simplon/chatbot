<?php

namespace App\Controller;

use App\Entity\Currency;
use App\Entity\Answer;
use App\Entity\Question;
use BotMan\BotMan\BotMan;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use BotMan\BotMan\Drivers\DriverManager;
use BotMan\BotMan\BotManFactory;

class BotController extends AbstractController
{
    /**
     * @Route("/message", name="bot_message")
     */
    public function messageAction(Request $request)
    {
        //Load web driver
        DriverManager::loadDriver(\BotMan\Drivers\Web\WebDriver::class);

        // Config web driver
        $botman = BotManFactory::create(
            [
                'web' => [
                    'matchingData' => [
                        'driver' => 'web',
                    ],
                ]
            ]

        );
        // Automatic answer when no results found
        $botman->fallback(function ($bot) {
            $message = $bot->getMessage();
            $bot->reply('Sorry, I don\'t understand: "' . $message->getText() . '"');
        });
        // Q&A with fixtures
        $questions = $this->getDoctrine()->getRepository(Question::class)->findAll();
        foreach ($questions as $question) {
            $answers = $this->getDoctrine()->getRepository(Answer::class)->findBy(array('question' => $question));
            foreach ($answers as $answer) {
                $botman->hears($question->getQuestionText(), function (Botman $bot) use ($answer) {
                    $bot->typesAndWaits(1);
                    $bot->reply($answer->getAnswerText());
                });
            }
        }
        //Q&A with crypto currencies name&price
        $results = $this->getDoctrine()->getRepository(Currency::class)->findAll();
        foreach ($results as $result) {
                $botman->hears($result->getName(), function (Botman $bot) use ($result) {
                    $bot->typesAndWaits(1);
                    $bot->reply($result->getPrice());
                });
            }
        // Answer with user name
        $botman->hears('My name is {name}', function ($bot, $name) {
            $bot->typesAndWaits(1);
            $bot->reply('Hello ' . $name .  ' 😊');
        });
        // Start listening
        $botman->listen();
        return new Response();
    }
}
