<?php

namespace App\Controller;

use App\Form\QuestionType;
use App\Entity\Question;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class QuestionController extends AbstractController
{
    /**
     * @Route("/add/question", name="question")
     */
    public function addAction(Request $request)
    {
        $question = new Question();
        $form = $this->createForm(QuestionType::class, $question);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($question);
            $em->flush();
            return new Response('Message ajouté !');
        }
        $formView = $form->createView();
        return $this->render("question/index.html.twig", array('form' => $formView));
    }
    /**
     * @Route("/list/question", name="list_question")
     */
    public function listAction()
    {
        $repository = $this->getDoctrine()->getRepository(Question::class);
        $questions = $repository->findAll();
        return $this->render("question/list.html.twig", array('questions' => $questions));
    }
    /**
     * @Route("/modify/question/{id}", name="modify_question")
     */
    public function edit(Request $request, question $question)
    {

        $form = $this->createForm(QuestionType::class, $question);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            return new Response('Message modifié !');
        }
        $formView = $form->createView();
        return $this->render("question/index.html.twig", array('form' => $formView));
    }
    /**
     * @Route("/delete/question/{id}", name="delete_question")
     */
    public function deleteAction(question $question)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($question);
        $em->flush();
        return new Response('Message supprimé !');
    }
}
