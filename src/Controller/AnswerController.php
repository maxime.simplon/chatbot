<?php

namespace App\Controller;

use App\Form\AnswerType;
use App\Entity\Answer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AnswerController extends AbstractController
{
    /**
     * @Route("/add/answer", name="add_answer")
     */
    public function addAction(Request $request)
    {
        $answer = new Answer();
        $form = $this->createForm(AnswerType::class, $answer);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($answer);
            $em->flush();
            return new Response('Message ajouté !');
        }
        $formView = $form->createView();
        return $this->render("answer/index.html.twig", array('form' => $formView));
    }
    /**
     * @Route("/list/answer", name="list_answer")
     */
    public function listAction()
    {
        $repository = $this->getDoctrine()->getRepository(answer::class);
        $answers = $repository->findAll();
        return $this->render("answer/list.html.twig", array('answers' => $answers));
    }
    /**
     * @Route("/modify/answer/{id}", name="modify_answer")
     */
    public function edit(Request $request, answer $answer)
    {
        $form = $this->createForm(answerType::class, $answer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            return new Response('Message modifié !');
        }
        $formView = $form->createView();
        return $this->render("answer/index.html.twig", array('form' => $formView));
    }
    /**
     * @Route("/delete/answer/{id}", name="delete_answer")
     */
    public function deleteAction(answer $answer)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($answer);
        $em->flush();
        return new Response('Message supprimé !');
    }
}
