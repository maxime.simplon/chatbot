<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;
use App\Entity\Currency;

class ScraperController extends AbstractController
{
    /**
     * @Route("/scraper", name="scraper")
     */
    public function scrape()
    {
        // Show a template
        return $this->render('scraper/index.html.twig', [
            'controller_name' => 'ScraperController',
        ]);
    }
    /**
     * @Route("/add/currency", name="add_currency")
     */
    public function addCurrency()
    {
        // Add crypto currency's name and price in database from coinmarketcap website
        $client = new Client();
        $res = $client->request('GET', 'https://coinmarketcap.com/');
        $domBody = $res->getBody();
        $crawler = new Crawler((string)$domBody);
        $entityManager = $this->getDoctrine()->getManager();
        $name = $crawler->filterXPath('//a[@class="currency-name-container link-secondary"]')->extract(array('_text'));
        $price = $crawler->filterXPath('//a[@class="price"]')->extract(array('_text'));
        for ($i = 0; $i < count($name); ++$i) {
            $currency = new Currency();
            $currency->setName($name[$i]);
            $currency->setPrice($price[$i]);
            $entityManager->persist($currency);
            $entityManager->flush();
        }
        return new Response('Entrée correctement ajoutée à la base de donnée');
    }

    /**
     * @Route("/delete/currency", name="delete_currency")
     */
    public function deleteCurrency()
    {
        $repository = $this->getDoctrine()->getRepository(Currency::class);
        $answers = $repository->findAll();
        $em = $this->getDoctrine()->getManager();
        foreach($answers as $answer){
        $em->remove($answer);
        $em->flush();
        }
        return new Response('Entrée correctement supprimée de la base de donnée');
    }
}
